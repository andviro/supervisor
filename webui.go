package main

import (
	"github.com/gin-gonic/gin"
	"html/template"
)

func _HTML(c *gin.Context, name string, data interface{}) {
	tpl, err := template.New(name).Parse(FSMustString(false, name))
	if err != nil {
		logger.Critical("Error rendering template: %v\n", err)
		c.AbortWithError(501, err)
		return
	}
	c.Writer.WriteHeader(200)
	tpl.Execute(c.Writer, data)
}

func (mgr *Manager) HandleUI(c *gin.Context) {
	// XXX
	var target RunStopDel

	grpName := c.Query("group")
	if grpName != "" {
		idx := mgr.GetGroup(grpName)
		if idx == -1 {
			c.AbortWithStatus(404)
			return
		}
		target = mgr.Groups[idx]
	} else {
		target = mgr
	}

	switch c.Query("action") {
	case "restart":
		target.Stop()
		target.Run()
	case "stop":
		target.Stop()
	case "run":
		target.Run()
	default:
		_HTML(c, "/templates/index.html", mgr)
		return
	}
	c.Redirect(307, "/?mode=ui")
}
