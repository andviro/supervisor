package main

import (
	"errors"
	"github.com/gin-gonic/gin"
	"regexp"
	"sync"
)

type ProcessGroup struct {
	parent             *Manager          `json:"-"`
	Name               string            `json:"name" form:"name" binding:"required"`
	Procs              []*Process        `json:"programs"`
	StartTimeout       int               `json:"startTimeout" form:"startTimeout"`
	MaxStartAttempts   int               `json:"maxStartAttempts" form:"maxStartAttempts"`
	MaxStopAttempts    int               `json:"maxStopAttempts" form:"maxStopAttempts"`
	MaxSocketAttempts  int               `json:"maxSocketAttempts" form:"maxSocketAttempts"`
	BackoffBaseTimeout int               `json:"backoffBaseTimeout" form:"backoffBaseTimeout"`
	SocketBaseTimeout  int               `json:"socketBaseTimeout" form:"socketBaseTimeout"`
	StopTimeout        int               `json:"stopTimeout" form:"stopTimeout"`
	LogStdout          bool              `json:"logStdout" form:"logStdout"`
	LogStderr          bool              `json:"logStderr" form:"logStderr"`
	LogTimeout         int               `json:"logTimeout" form:"logTimeout"`
	LogBadRegexp       string            `json:"logBadRegexp" form:"LogBadRegexp"`
	LogRepeatRegexp    string            `json:"logRepeatRegexp" form:"LogRepeatRegexp"`
	MaxBadLogLines     int               `json:"maxBadLogLines" form:"maxBadLogLines"`
	MaxRepeatLogLines  int               `json:"maxRepeatLogLines" form:"maxRepeatLogLines"`
	logCompiledRe      *regexp.Regexp    `json:"-"`
	logRepeatRe        *regexp.Regexp    `json:"-"`
	Hooks              map[string]string `json:"hooks,omitempty" form:"hooks"`
	sync.RWMutex       `json:"-"`
	sync.WaitGroup     `json:"-"`
}

func NewGroup(name string) *ProcessGroup {
	res := ProcessGroup{Name: name}
	return &res
}

func (grp *ProcessGroup) Init(parent *Manager) {
	if grp.StartTimeout == 0 {
		grp.StartTimeout = 3
	}
	if grp.MaxStartAttempts == 0 {
		grp.MaxStartAttempts = 5
	}
	if grp.MaxStopAttempts == 0 {
		grp.MaxStopAttempts = 5
	}
	if grp.BackoffBaseTimeout == 0 {
		grp.BackoffBaseTimeout = 2
	}
	if grp.StopTimeout == 0 {
		grp.StopTimeout = 10
	}
	if grp.SocketBaseTimeout == 0 {
		grp.SocketBaseTimeout = 1
	}
	if grp.MaxSocketAttempts == 0 {
		grp.MaxSocketAttempts = 15
	}
	if grp.LogBadRegexp != "" {
		re, err := regexp.Compile(grp.LogBadRegexp)
		if err != nil {
			logger.Critical("Error compiling regexp for group %s: %v\n", grp.Name, err)
		} else {
			grp.logCompiledRe = re
		}
	}
	if grp.LogRepeatRegexp != "" {
		re, err := regexp.Compile(grp.LogRepeatRegexp)
		if err != nil {
			logger.Critical("Error compiling repeat regexp for group %s: %v\n", grp.Name, err)
		} else {
			grp.logRepeatRe = re
		}
	}
	grp.parent = parent
}

func (grp *ProcessGroup) GetProcess(name string) int {
	for i, proc := range grp.Procs {
		if proc.Name == name {
			return i
		}
	}
	return -1
}

func (grp *ProcessGroup) AddProcess(newProc *Process) {
	logger.Info("Adding process %s\n", newProc.Name)
	grp.Lock()
	defer grp.Unlock()
	if idx := grp.GetProcess(newProc.Name); idx != -1 {
		logger.Warning("Process '%s' already exists\n", newProc.Name)
		return
	}
	newProc.Init(grp)
	grp.Procs = append(grp.Procs, newProc)
	newProc.Run()
}

func (grp *ProcessGroup) Stop() {
	logger.Info("Stopping group %s\n", grp.Name)
	for _, proc := range grp.Procs {
		proc.Stop()
	}
	logger.Info("Group %s stopped\n", grp.Name)
}

func (grp *ProcessGroup) Delete() {
	logger.Info("Deleting group %s\n", grp.Name)
	grp.Lock()
	defer grp.Unlock()
	grp.Stop()
	grp.Procs = nil
	i := grp.parent.GetGroup(grp.Name)
	if i == -1 {
		logger.Critical("Group %s not found in parent manager, delete ignored\n", grp.Name)
		return
	}
	grp.parent.Groups = append(grp.parent.Groups[:i], grp.parent.Groups[i+1:]...)
	logger.Info("Group %s deleted from parent manager\n", grp.Name)
}

func (grp *ProcessGroup) Run() {
	logger.Info("Starting group %s\n", grp.Name)
	for _, proc := range grp.Procs {
		proc.Run()
	}
}

func (grp *ProcessGroup) WithProcess(f func(c *gin.Context, proc *Process)) gin.HandlerFunc {
	return func(c *gin.Context) {
		processName := c.Params.ByName("process")
		procIdx := grp.GetProcess(processName)
		if procIdx == -1 {
			c.Error(errors.New("Process " + processName + " not found"))
			c.JSON(404, gin.H{"errors": c.Errors})
			return
		}
		proc := grp.Procs[procIdx]
		f(c, proc)
	}
}
