package main

import (
	"github.com/cratonica/trayhost"
	"os"
	"runtime"
)

func (mgr *Manager) InstallIcon(port string) chan bool {
	res := make(chan bool, 1)
	if runtime.GOOS == "linux" && os.Getenv("DISPLAY") == "" {
		return res
	}
	go func() {
		runtime.LockOSThread()
		trayhost.SetUrl("http://" + port + "/?mode=ui")
		trayhost.EnterLoop("Supervisor "+version, iconData)
		res <- true
	}()
	return res
}
