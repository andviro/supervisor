package main

import (
	"flag"
	"github.com/go-errors/errors"
	"os"
	"os/signal"
	"syscall"
)

const version string = "0.1.13"

func (mgr *Manager) Leave() {
	if r := recover(); r != nil {
		logger.Printf(errors.Wrap(r, 2).ErrorStack())
	}
	mgr.Save(nil)
	mgr.Stop()
	logger.Info("Shutting down")
}

func main() {
	mgr := NewManager()
	defer mgr.Leave()
	logger.Info("Supervisor version %s initializing\n", version)
	port := flag.String("port", "localhost:8910", "Server address")
	authUser := flag.String("user", "", "HTTP auth user")
	authPassword := flag.String("password", "", "HTTP auth password")
	flag.Parse()
	server, err := mgr.CreateAPI(*port, *authUser, *authPassword)
	if err != nil {
		logger.Fatal(err)
	}
	mgr.Restore()
	icon := mgr.InstallIcon(*port)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGKILL)
	defer signal.Stop(interrupt)
	select {
	case sig := <-interrupt:
		logger.Warning("Signal received: %v\n", sig)
	case err := <-server:
		logger.Critical("API server exit with error %v\n", err)
	case <-icon:
		logger.Info("Exit through tray menu\n")
	}
}
