package main

import (
	"sync"
)

type Manager struct {
	Version      string          `json:"version"`
	stop         chan bool       `json:"-"`
	ready        chan bool       `json "-"`
	wg           sync.WaitGroup  `json:"-"`
	StateFile    string          `json:"-"`
	Groups       []*ProcessGroup `json:"groups"`
	sync.RWMutex `json:"-"`
}

func NewManager() *Manager {
	return &Manager{
		stop:      make(chan bool),
		ready:     make(chan bool),
		Version:   version,
		StateFile: "supervisor.json",
	}
}

func (mgr *Manager) GetGroup(name string) int {
	for i, grp := range mgr.Groups {
		if grp.Name == name {
			return i
		}
	}
	return -1
}

func (mgr *Manager) AddGroup(newGrp *ProcessGroup) {
	logger.Info("Adding group %s\n", newGrp.Name)
	mgr.Lock()
	defer mgr.Unlock()
	if grpIdx := mgr.GetGroup(newGrp.Name); grpIdx != -1 {
		logger.Warning("Group '%s' already exists\n", newGrp.Name)
		return
	}
	newGrp.Init(mgr)
	mgr.Groups = append(mgr.Groups, newGrp)
}

func (mgr *Manager) Stop() {
	logger.Info("Stopping manager\n")
	for _, grp := range mgr.Groups {
		grp.Stop()
	}
	logger.Info("Manager stopped\n")
}

func (mgr *Manager) Delete() {
	logger.Info("Deleting manager\n")
	mgr.Lock()
	defer mgr.Unlock()
	mgr.Stop()
	mgr.Groups = nil
	logger.Info("Manager cleared\n")
}

func (mgr *Manager) Run() {
	logger.Info("Starting manager\n")
	for _, grp := range mgr.Groups {
		grp.Run()
	}
}
