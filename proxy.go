package main

import (
	"net/http"
	"net/http/httputil"
	"net/url"
)

func (proc *Process) makeReverseProxy() *httputil.ReverseProxy {
	target, err := url.Parse(proc.ProxyURL)
	if err != nil {
		logger.Critical("Could not parse URL for process %s: %v\n", proc.Name, err)
		return nil
	}
	targetQuery := target.RawQuery
	cutLen := len(proc.Group.Name) + len(proc.Name) + len("proxy") + 4
	director := func(req *http.Request) {
		cutPath := req.URL.Path[cutLen:]
		req.URL.Scheme = target.Scheme
		req.URL.Host = target.Host
		req.URL.Path = singleJoiningSlash(target.Path, cutPath)
		logger.Debug("Proxying request to '%s'\n", req.URL.Path)
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
	}
	return &httputil.ReverseProxy{Director: director}
}
