package main

import (
	"net"
	"time"
)

func (proc *Process) waitSocket() chan bool {
	res := make(chan bool, 1)
	if proc.WatchSocket == "" {
		return res
	}
	go func() {
		for try := 0; try < proc.Group.MaxSocketAttempts; try++ {
			conn, err := net.Dial("tcp", proc.WatchSocket)
			if err == nil {
				conn.Close()
				res <- true
				return
			}
			time.Sleep(time.Duration(proc.Group.SocketBaseTimeout) * time.Second)
		}
		res <- false
	}()
	return res
}
