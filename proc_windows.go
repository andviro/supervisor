// +build windows

package main

import (
	"syscall"
)

func (proc *Process) setupWindow() {
	proc.command.SysProcAttr = new(syscall.SysProcAttr)
	proc.command.SysProcAttr.HideWindow = true
}
