package main

import (
	"bytes"
	"github.com/franela/goreq"
	"net/http/httputil"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"text/template"
	"time"
)

func NewTimeout(seconds int) chan bool {
	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(time.Duration(seconds) * time.Second)
		timeout <- true
	}()
	return timeout
}

type Process struct {
	Name          string                 `json:"name" form:"name" binding:"required"`
	Cmd           string                 `json:"command" form:"command" binding:"required"`
	Args          []string               `json:"args" form:"args"`
	Dir           string                 `json:"dir" form:"dir"`
	ProxyURL      string                 `json:"proxyURL" form:"proxyURL"`
	WatchSocket   string                 `json:"watchSocket" form:"watchSocket"`
	State         string                 `json:"state"`
	Try           int                    `json:"try"`
	StopAttempt   int                    `json:"stopAttempt"`
	StartAttempt  int                    `json:"startAttempt"`
	Err           string                 `json:"err,omitempty"`
	Group         *ProcessGroup          `json:"-"`
	proxyClient   *httputil.ReverseProxy `json:"-"`
	result        chan error             `json:"-"`
	cancel        chan bool              `json:"-"`
	finished      chan bool              `json:"-"`
	keepalive     chan bool              `json:"-"`
	logEvent      chan LogEvent          `json:"-"`
	numBadMatches int                    `json:"-"`
	numRepeats    int                    `json:"-"`
	lastMatch     string                 `json:"-"`
	LogStdout     string                 `json:"-"`
	LogStderr     string                 `json:"-"`
	state         ProcessState           `json:"-"`
	command       *exec.Cmd              `json:"-"`
	sync.RWMutex  `json:"-"`
}

type ProcessState func() ProcessState

func (proc *Process) Init(parent *ProcessGroup) {
	proc.Group = parent
	proc.cancel = make(chan bool, 1)
	proc.finished = make(chan bool, 1)
	proc.keepalive = make(chan bool, 2)
	proc.logEvent = make(chan LogEvent, 2)
	proc.result = make(chan error)
	if proc.ProxyURL == "" {
		return
	}
	proc.proxyClient = proc.makeReverseProxy()
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func (proc *Process) Date(layout string) string {
	d := time.Now()
	return d.Format(layout)
}

func (proc *Process) SetError(err error) {
	if err != nil {
		proc.Err = err.Error()
	} else {
		proc.Err = ""
	}
}

func NewProcess(name string, cmd string, args []string) *Process {
	res := Process{
		Name: name,
		Cmd:  cmd,
		Args: args,
	}
	return &res
}

func (proc *Process) updateStateName() {
	// XXX Black magic XXX
	state := strings.Split(runtime.FuncForPC(reflect.ValueOf(proc.state).Pointer()).Name(), ".")
	proc.State = strings.ToUpper(state[len(state)-1])
	proc.State = proc.State[:len(proc.State)-4]
	logger.Info("Process %s entering state '%s' (error = '%s')\n", proc.Name, proc.State, proc.Err)
}

func (proc *Process) runStateHook(state string) {
	hook, ok := proc.Group.Hooks[state]
	if !ok {
		return
	}
	hookUri := proc.EvalInContext(hook)
	logger.Info("Sending hook '%s' for process %s to URL %s\n", state, proc.Name, hookUri)
	res, err := goreq.Request{
		Uri:         hookUri,
		Method:      "POST",
		Body:        proc,
		ContentType: "application/json",
		ShowDebug:   false,
	}.Do()
	if err != nil {
		logger.Critical("Error running hook '%s' for process %s: %v\n", state, proc.Name, err)
		return
	}
	logger.Info("Hook '%s' for process %s finished with status code %d\n", state, proc.Name, res.StatusCode)
}

func (proc *Process) Run() {
	defer proc.Unlock()
	proc.Lock()
	if proc.state != nil {
		logger.Warning("Process %s already running\n", proc.Name)
		return
	}
	if proc.Group == nil {
		logger.Critical("Process %s is not attached to group, not starting\n", proc.Name)
		return
	}
	proc.state = proc.Starting
	proc.Err = ""
	proc.StartAttempt = 0
	proc.StopAttempt = 0
	go func() {
		for proc.state != nil {
			proc.updateStateName()
			go proc.runStateHook(proc.State)
			proc.state = proc.state()
		}
		proc.finished <- true
		logger.Info("Process %s shut down (error = '%s')\n", proc.Name, proc.Err)
	}()
}

func (proc *Process) Stop() {
	defer proc.Unlock()
	proc.Lock()
	if proc.state == nil {
		logger.Warning("Process %s not running\n", proc.Name)
		return
	}
	proc.cancel <- true
	<-proc.finished
}

func (proc *Process) Delete() {
	logger.Info("Deleting process %s from group %s\n", proc.Name, proc.Group.Name)
	proc.Group.Lock()
	defer proc.Group.Unlock()
	i := proc.Group.GetProcess(proc.Name)
	proc.Stop()
	if i == -1 {
		logger.Critical("Process %s not found in parent group, delete ignored\n", proc.Name)
		return
	}
	proc.Group.Procs = append(proc.Group.Procs[:i], proc.Group.Procs[i+1:]...)
	logger.Info("Process %s deleted from parent group\n", proc.Name)
}

func (proc *Process) Starting() ProcessState {
	proc.command = exec.Command(proc.Cmd, proc.EvalArgs()...)
	if proc.Dir != "" {
		proc.command.Dir = proc.EvalInContext(proc.Dir)
		logger.Info("Starting process %s in working directory %s\n", proc.Name, proc.command.Dir)
	}
	proc.Try++
	proc.setupWindow()
	proc.numBadMatches = 0
	proc.numRepeats = 0

	if err := proc.setupLogging(); err != nil {
		proc.SetError(err)
		return proc.Failed
	}

	if err := proc.command.Start(); err != nil {
		proc.SetError(err)
		return proc.Failed
	}

	go func() {
		res := proc.command.Wait()
		proc.result <- res
	}()

	timeout := NewTimeout(proc.Group.StartTimeout)
	select {
	case <-proc.cancel:
		return proc.Stopping
	case err := <-proc.result:
		proc.SetError(err)
		proc.StartAttempt++
		return proc.Backoff
	case <-timeout:
		proc.SetError(nil)
		proc.StartAttempt = 0
	}
	return proc.Running
}

func (proc *Process) EvalInContext(s string) string {
	var data []byte
	buf := bytes.NewBuffer(data)
	tpl, err := template.New("s").Parse(s)
	if err != nil {
		logger.Critical("Error parsing templated argument: %v\n", err)
		return s
	}
	if err := tpl.Execute(buf, proc); err != nil {
		logger.Critical("Error executing templated argument: %v\n", err)
		return s
	}
	return buf.String()
}

func (proc *Process) EvalArgs() []string {
	var res []string
	for _, s := range proc.Args {
		res = append(res, proc.EvalInContext(s))
	}
	return res
}

func (proc *Process) Backoff() ProcessState {
	if proc.StartAttempt > proc.Group.MaxStartAttempts {
		return proc.Failed
	}

	timeout := NewTimeout(proc.Group.BackoffBaseTimeout * proc.StartAttempt)
	select {
	case <-proc.cancel:
		return proc.Stopping
	case <-timeout:
		proc.SetError(nil)
	}
	return proc.Starting
}

func (proc *Process) Stopping() ProcessState {
	if proc.StopAttempt > proc.Group.MaxStopAttempts {
		proc.Err = "Maximum stop attempt count reached"
		return proc.Zombie
	}

	err := proc.command.Process.Signal(os.Kill)
	if err != nil {
		proc.SetError(err)
		return proc.Failed
	}

	timeout := NewTimeout(proc.Group.StopTimeout)
	select {
	case err := <-proc.result:
		proc.SetError(err)
		break
	case <-timeout:
		proc.SetError(nil)
		proc.StopAttempt++
		return proc.Stopping
	}
	return proc.Stopped
}

func (proc *Process) Reanimating() ProcessState {
	if proc.StopAttempt > proc.Group.MaxStopAttempts {
		proc.Err = "Maximum stop attempt count reached"
		return proc.Zombie
	}

	err := proc.command.Process.Signal(os.Kill)
	if err != nil {
		proc.SetError(err)
		return proc.Failed
	}

	timeout := NewTimeout(proc.Group.StopTimeout)
	select {
	case err := <-proc.result:
		proc.SetError(err)
		break
	case <-timeout:
		proc.SetError(nil)
		proc.StopAttempt++
		return proc.Reanimating
	}
	return proc.Starting
}

func (proc *Process) Running() ProcessState {
	to := proc.Group.LogTimeout
	if to == 0 {
		to = 10
	}
	socketWatch := proc.waitSocket()
	for {
		timeout := NewTimeout(to)
		select {
		case <-proc.cancel:
			return proc.Stopping
		case logEvt := <-proc.logEvent:
			switch logEvt {
			case EvtBadLine:
				proc.numBadMatches++
				if proc.numBadMatches > proc.Group.MaxBadLogLines {
					logger.Critical("Process %s sent %d bad lines to log\n", proc.Name, proc.numBadMatches)
					return proc.Reanimating
				}
			case EvtRepeat:
				logger.Critical("Process %s sent '%s' to log %d times\n", proc.Name, proc.lastMatch, proc.numRepeats)
				return proc.Reanimating
			}
		case <-proc.keepalive:
			break
		case sockRes := <-socketWatch:
			if sockRes {
				logger.Info("Socket %s is ready for process %s\n", proc.WatchSocket, proc.Name)
				go proc.runStateHook("SOCKET_READY")
			} else {
				logger.Info("Socket %s is failed for process %s\n", proc.WatchSocket, proc.Name)
				go proc.runStateHook("SOCKET_TIMEOUT")
			}
		case <-timeout:
			if proc.Group.LogTimeout != 0 && (proc.Group.LogStderr || proc.Group.LogStdout) {
				logger.Critical("Process %s is not breathing for %d seconds!\n", proc.Name, to)
				return proc.Reanimating
			}
		case err := <-proc.result:
			proc.SetError(err)
			logger.Warning("Process %s terminated (error='%v')\n", proc.Name, proc.Err)
			return proc.Restarting
		}
	}
	return nil
}

func (proc *Process) Restarting() ProcessState {
	return proc.Starting
}

func (proc *Process) Stopped() ProcessState {
	return nil
}

func (proc *Process) Failed() ProcessState {
	return nil
}

func (proc *Process) Zombie() ProcessState {
	return nil
}
