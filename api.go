package main

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http/pprof"
	"os"
)

func init() {
	gin.SetMode(gin.ReleaseMode)
}

func (mgr *Manager) List(c *gin.Context) { c.JSON(200, mgr) }

func (mgr *Manager) CreateGroup(c *gin.Context) {
	var arg ProcessGroup
	if err := c.Bind(&arg); err != nil {
		logger.Critical("Could not load group descriptor: %v\n", err)
		c.JSON(400, gin.H{"errors": c.Errors})
		return
	}
	mgr.AddGroup(&arg)
	c.JSON(201, arg)
}

func (mgr *Manager) WithGroup(f func(c *gin.Context, grp *ProcessGroup)) gin.HandlerFunc {
	return func(c *gin.Context) {
		groupName := c.Params.ByName("group")
		grpIdx := mgr.GetGroup(groupName)
		if grpIdx == -1 {
			c.Error(errors.New("Group " + groupName + " not found"))
			c.JSON(404, gin.H{"errors": c.Errors})
			return
		}
		grp := mgr.Groups[grpIdx]
		f(c, grp)
	}
}

func (grp *ProcessGroup) CreateProcess(c *gin.Context) {
	var proc Process
	if err := c.Bind(&proc); err != nil {
		logger.Critical("Could not load process descriptor: %v\n", err)
		c.JSON(400, gin.H{"errors": c.Errors})
		return
	}
	grp.AddProcess(&proc)
	c.JSON(201, proc)
}

func (mgr *Manager) Save(c *gin.Context) {
	if c != nil {
		c.Next()
	}
	mgr.Lock()
	defer mgr.Unlock()
	logger.Info("Saving manager\n")
	fp, err := os.Create(mgr.StateFile)
	if err != nil {
		logger.Fatal(err)
	}
	defer fp.Close()
	json.NewEncoder(fp).Encode(mgr)
}

func (mgr *Manager) Restore() error {
	mgr.Lock()
	defer mgr.Unlock()
	defer close(mgr.ready)

	logger.Info("Restoring manager state\n")
	fp, err := os.Open(mgr.StateFile)
	if err != nil {
		logger.Critical("Error opening file: %v\n", err)
		return err
	}
	defer fp.Close()
	if err := json.NewDecoder(fp).Decode(mgr); err != nil {
		logger.Critical("Error decoding json: %v\n", err)
		return err
	}
	mgr.Version = version
	for _, grp := range mgr.Groups {
		grp.Init(mgr)
		for _, proc := range grp.Procs {
			proc.Init(grp)
			proc.Run()
		}
	}
	return nil
}

type RunStopDel interface {
	Run()
	Stop()
	Delete()
}

func ApplyAction(c *gin.Context, target RunStopDel) {
	switch c.Request.URL.Query().Get("action") {
	case "run":
		target.Run()
	case "stop":
		target.Stop()
	case "restart":
		target.Stop()
		target.Run()
	case "delete":
		target.Delete()
	}
	c.JSON(200, target)
}

func (mgr *Manager) waitReadyMid(c *gin.Context) {
	<-mgr.ready
}

func addMethods(r *gin.Engine, relativePath string, handlers ...gin.HandlerFunc) {
	for _, method := range []string{"GET", "POST", "DELETE", "PUT", "PATCH"} {
		r.Handle(method, relativePath, handlers...)
	}
}

func (mgr *Manager) CreateAPI(addr, user, password string) (chan error, error) {
	res := make(chan error, 1)
	r := gin.New()
	r.Use(gin.Recovery(), PlainLogger())
	if user != "" {
		authMid := gin.BasicAuth(gin.Accounts{user: password})
		r.Use(authMid)
	}
	r.Use(mgr.waitReadyMid)
	r.GET("/", func(c *gin.Context) {
		switch c.Query("mode") {
		case "ui":
			mgr.HandleUI(c)
		case "log":
			c.File("supervisor.log")
		default:
			ApplyAction(c, mgr)
		}
	})
	r.POST("/", mgr.Save, mgr.CreateGroup)
	r.GET("/:group", mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		ApplyAction(c, grp)
	}))
	r.POST("/:group", mgr.Save, mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		grp.CreateProcess(c)
	}))
	r.GET("/:group/:process", mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		grp.WithProcess(func(c *gin.Context, proc *Process) {
			ApplyAction(c, proc)
		})(c)
	}))
	r.GET("/:group/:process/stdout", mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		grp.WithProcess(func(c *gin.Context, proc *Process) {
			if proc.LogStdout == "" {
				c.String(404, "No log for this process")
				return
			}
			c.File(proc.LogStdout)
		})(c)
	}))
	r.GET("/:group/:process/stderr", mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		grp.WithProcess(func(c *gin.Context, proc *Process) {
			if proc.LogStderr == "" {
				c.String(404, "No log for this process")
				return
			}
			c.File(proc.LogStderr)
		})(c)
	}))
	addMethods(r, "/:group/:process/proxy/*rest", mgr.WithGroup(func(c *gin.Context, grp *ProcessGroup) {
		grp.WithProcess(func(c *gin.Context, proc *Process) {
			if proc.State != "RUNNING" {
				c.Error(errors.New("Process " + proc.Name + " not running"))
				c.JSON(401, gin.H{"errors": c.Errors})
				return
			}
			if proc.proxyClient == nil {
				c.Error(errors.New("Process " + proc.Name + " have no associated proxy"))
				c.JSON(404, gin.H{"errors": c.Errors})
				return
			}
			proc.proxyClient.ServeHTTP(c.Writer, c.Request)
		})(c)
	}))
	r.GET("/:group/:process/profile", func(c *gin.Context) {
		pprof.Profile(c.Writer, c.Request)
	})
	go func() {
		err := r.Run(addr)
		res <- err
	}()
	timeout := NewTimeout(2)
	select {
	case <-timeout:
		return res, nil
	case err := <-res:
		return nil, err
	}
	return res, nil
}
