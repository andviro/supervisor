package main

import (
	"bytes"
)

func scanForDelimiter(data []byte) int {
	i := bytes.IndexByte(data, '\n')
	j := bytes.IndexByte(data, '\r')
	switch true {
	case j < 0:
		return i
	case i < 0:
		return j
	case j < i:
		return j
	}
	return i
}

func ScanLines2(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := scanForDelimiter(data); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, data[0:i], nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}
