package main

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/kardianos/osext"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"log"
	"os"
	"time"
)

type LogLevel int
type LogEvent int

type SimpleLogger struct {
	Level LogLevel
	*log.Logger
}

const (
	LevelCritical LogLevel = 0 + iota
	LevelWarning
	LevelInfo
	LevelDebug
)

const (
	EvtBadLine LogEvent = 0 + iota
	EvtRepeat
)

func (logger *SimpleLogger) output(level LogLevel, format string, args []interface{}) {
	if level > logger.Level {
		return
	}
	var prefix string
	switch level {
	case LevelDebug:
		prefix = "DBUG "
	case LevelInfo:
		prefix = "INFO "
	case LevelWarning:
		prefix = "WARN "
	case LevelCritical:
		prefix = "CRIT "
	}
	logger.Printf(prefix+format, args...)
}

func (logger *SimpleLogger) Debug(format string, args ...interface{}) {
	logger.output(LevelDebug, format, args)
}

func (logger *SimpleLogger) Info(format string, args ...interface{}) {
	logger.output(LevelInfo, format, args)
}

func (logger *SimpleLogger) Warning(format string, args ...interface{}) {
	logger.output(LevelWarning, format, args)
}

func (logger *SimpleLogger) Critical(format string, args ...interface{}) {
	logger.output(LevelCritical, format, args)
}

var logger *SimpleLogger

func NewLogger(level LogLevel) *SimpleLogger {
	logger = &SimpleLogger{Level: level}
	logger.Logger = log.New(&lumberjack.Logger{
		Filename:   "supervisor.log",
		MaxSize:    10,
		MaxBackups: 10,
	}, "", log.Ldate|log.Ltime)
	return logger
}

func init() {
	folderPath, err := osext.ExecutableFolder()
	if err != nil {
		log.Fatal(err)
	}
	if err := os.Chdir(folderPath); err != nil {
		log.Fatal(err)
	}
	logger = NewLogger(LevelInfo)
}

func PlainLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Next()
		end := time.Now()
		latency := end.Sub(start)
		statusCode := c.Writer.Status()
		clientIP := c.ClientIP()
		method := c.Request.Method

		logger.Info("%3d %s %s '%s' %s %v\n%s",
			statusCode,
			method,
			c.Request.URL.Path,
			c.Request.URL.RawQuery,
			clientIP,
			latency,
			c.Errors.String(),
		)
	}
}

func (proc *Process) setupLogging() error {
	if proc.Group.LogStdout {
		logFile := fmt.Sprintf("%s.%s.stdout.log", proc.Group.Name, proc.Name)
		proc.LogStdout = logFile
		pipe, err := proc.command.StdoutPipe()
		if err != nil {
			return err
		}
		go proc.LogOutput(pipe, logFile)

	}
	if proc.Group.LogStderr {
		logFile := fmt.Sprintf("%s.%s.stderr.log", proc.Group.Name, proc.Name)
		proc.LogStderr = logFile
		pipe, err := proc.command.StderrPipe()
		if err != nil {
			return err
		}
		go proc.LogOutput(pipe, logFile)
	}
	return nil
}

func (proc *Process) LogOutput(pipe io.ReadCloser, logFile string) {
	scanner := bufio.NewScanner(pipe)
	scanner.Split(ScanLines2)
	proclog := log.New(&lumberjack.Logger{
		Filename:   logFile,
		MaxSize:    10,
		MaxBackups: 10,
	}, "", log.Ldate|log.Ltime)

	for scanner.Scan() {
		line := scanner.Text()
		if len(line) > 0 {
			proclog.Println(line)
		}
		if proc.Group.logCompiledRe != nil && proc.Group.logCompiledRe.FindString(line) != "" {
			proc.logEvent <- EvtBadLine
			continue
		}
		if proc.Group.logRepeatRe != nil {
			match := proc.Group.logRepeatRe.FindString(line)
			if match != "" && match == proc.lastMatch {
				proc.numRepeats++
				if proc.numRepeats > proc.Group.MaxRepeatLogLines {
					proc.logEvent <- EvtRepeat
					continue
				}
			} else {
				proc.lastMatch = match
				proc.numRepeats = 0
			}
		}
		proc.keepalive <- true
	}
}
